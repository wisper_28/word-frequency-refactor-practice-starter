import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String SPACE_BREAK = "\\s+";
    public static final int INIT_COUNT = 1;

    public String getResult(String inputStr) {


        try {
            List<Input> inputList = transFormToInput(inputStr);

            Map<String, List<Input>> map = getListMap(inputList);

            List<Input> list = new ArrayList<>();
            for (Map.Entry<String, List<Input>> entry : map.entrySet()) {
                Input input = new Input(entry.getKey(), entry.getValue().size());
                list.add(input);
            }
            inputList = list;

            inputList.sort((word1, word2) -> word2.getWordCount() - word1.getWordCount());

            return getResultString(inputList);
        } catch (Exception e) {
            return "Calculate Error";
        }

    }

    private String getResultString(List<Input> inputList) {
        StringJoiner joiner = new StringJoiner("\n");
        inputList.stream()
                .map(word -> joiner.add(word.getValue() + " " + word.getWordCount()))
                .collect(Collectors.toList());
        return joiner.toString();
    }

    private static List<Input> transFormToInput(String sentence) {
        //split the input string with 1 to n pieces of spaces
        String[] words = sentence.split(SPACE_BREAK);
        return Arrays.stream(words)
                .map(word -> new Input(word, INIT_COUNT))
                .collect(Collectors.toList());
    }


    private Map<String, List<Input>> getListMap(List<Input> inputList) {
        Map<String, List<Input>> listMap = new HashMap<>();
        inputList.forEach(input -> {
            if (listMap.containsKey(input.getValue())) {
                listMap.get(input.getValue())
                        .add(input);
            } else {
                List<Input> list = new ArrayList<>();
                list.add(input);
                listMap.put(input.getValue(), list);
            }
        });
        return listMap;
    }


}
