O:

This morning, I first conducted a code review of the weekend work, and then shared the design pattern as a group. In the
afternoon, I first introduced the relevant knowledge of code reconstruction, introduced some common code smell, and then
practiced the response.

R:

Today I feel I have gained a lot. In the morning, I saw the shortcomings in my code by reading the code of my classmates
and sharing the code of my classmates. At the same time, we learned the strategy pattern and observer pattern shared by
the other two groups.

I:

Through today's learning, I have a deep understanding of the three design patterns introduced and code refactoring.

D:

In the future development, I will think about whether my code needs to use a certain design pattern, and I will attach
importance to code reconstruction, apply what I have learned today to future development, and use code reconstruction to
enhance the readability and extensibility of my code.